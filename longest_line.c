#include <stdio.h>
#define MAXLINE 4

int linesize(char line[], int maxline);

int main(){
	char line[MAXLINE];
	int max = 0;
	int size;
	while((size = linesize(line,MAXLINE)) != 0){
		printf("%s\n", line);
		printf("%d\n",size);
	}

}


int linesize(char line[],int maxline){
	int c;
	int i;
	for(i = 0; (c=getchar())!='\n' && c !=EOF; i++){
		if (i<maxline-1){
			line[i] = c;
		}
	}
	if (c == '\n'){
		line[i] = c;
		i++;
	}

	line[i] = '\0';
	return i;
}
