#include <stdio.h>

unsigned setbits(unsigned x, int p, int n, unsigned y){
	unsigned rightmost_y = y & ~(~0 << n);
	unsigned mask1 = rightmost_y << (p-n);
	unsigned elon_mask = ~0<<(p-n) & ~(~0<<p);
	unsigned mask0 = ~(~mask1 & elon_mask);
	return (x & mask0) | mask1;
	// 0101 | 0110 = 0111
	// 0111 & 0010 = 0010
}

int main(){
	printf("%x",setbits(0x5,3,2,0x1));
}


/* y = 0001 */
/* mask0 = ~(~rightmost_y and elon_mask) */
/* x = 0101 */

/* rightmost_y = 0010 */
/* mask1 = rightmost_y */

/* mask0 = 1011 => x and mask0 = 0001 = new_x */
/* mask1 = 0010 => new_x or mask1 = 0011 */

/* y = 1000111 */
/* n = 2 */
/* p=4 */
/* rightmost_y = 0000011 */
/* shifteamos p-n posiciones: 0...1100 */


/* x O y = y */

/* O puede ser or si x=0 */
/* O puede ser and si x = 1 */

/* queremos llevar los bits de x a 1s */
/* mask = 0...1...10...0 */
/* n 1s y n-p ceros */
/* x or mask */

























