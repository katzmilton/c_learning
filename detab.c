#include <stdio.h>
#define n 8

int main(){
	int i =0;
	int c;
	int spaces = 0;
	while ((c=getchar())!=EOF){
		if (c != ' ' && c != '\t' && c!='\n'){
			if (spaces!=0){
				for (int j =0;j<spaces;j++){
					putchar(' ');
				}
				spaces = 0;
			}

			putchar(c);
			i+=1;

			if (i==n){
				i=0;
			}

		} else if(c==' '){
			i+=1;

			if (i==n){
				i=0;
				putchar('\t');
				spaces = 0;
			} else{
				spaces += 1;
			}
		} else if(c=='\t'){
			i =0;
			spaces=0;
			putchar('\t');
		} else if(c == '\n'){
			i = 0;
			spaces = 0;
			putchar('\n');
		}

	}
}
