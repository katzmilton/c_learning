#include <stdio.h>
/* print Fahrenheit-Celsius table */
int main()
{
	int fahr;
	float cel;
	cel = 0;
	for (fahr = 0; cel <= 300; fahr = fahr + 20){
		cel = (5.0/9.0)*(fahr-32); 
		printf("%3d %6.1f\n", fahr, cel);
	}
} 
