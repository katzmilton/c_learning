#include <stdio.h>

int main()
{
	int blank = 0;
	for(int c = getchar(); c != EOF; c = getchar()){
		if (blank == 0){
			putchar(c);
			putchar('\b');
			if (c == ' ')
				blank = 1;
		}
		else if(c!=' '){
			putchar(c);
			blank = 0;
		}
	}
}

