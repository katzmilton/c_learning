#include <stdio.h>
#define FIRST 'a'
#define LAST 'z'
#define LENGTH LAST-FIRST+1
#define LINES 10

int main()
{
	float digit_cnt[LENGTH];
	int c;
	float max = 0;
	float scaling;

	for (int i = 0; i<LENGTH; i++){
		digit_cnt[i] = 0;
	}

	while((c = getchar()) != EOF){
		if (c <= LAST & c>=FIRST){
			digit_cnt[c - FIRST] += 1;
		}
	}

	for (int i = 0; i<LENGTH; i++){
		if (digit_cnt[i] > max){
			max = digit_cnt[i];
		}
	}

	scaling = max/LINES;

	for (int i = 0; i<LENGTH; i++){
		digit_cnt[i] = digit_cnt[i]/scaling;
	}
	max = max/scaling;
		
	for (int i = 0; i<=max; i++){
		for (int j = 0; j<LENGTH; j++){
			if (max - digit_cnt[j] >= i){
				printf("  ");
			}
			else printf("| ");
		}
		printf("\n");
	}
	for (int i = 0; i<LENGTH; i++){
		printf("%c ",i + FIRST);
	}
	printf("\n");

}
